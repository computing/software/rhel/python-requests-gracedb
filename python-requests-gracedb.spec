%global srcname requests-gracedb
%global srcname_underscores %(echo "%{srcname}" | tr - _)
%global srcname_first_letter %(echo "%{srcname}" | cut -c1)

Name:           python-%{srcname}
Version:        0.1.3
Release:        1%{?dist}
Summary:        Generic connection pooling HTTP client for GraceDB and similar services

License:        GPLv2+
URL:            https://pypi.org/project/%{srcname}/
Source0:        https://files.pythonhosted.org/packages/py2.py3/%{srcname_first_letter}/%{srcname}/%{srcname_underscores}-%{version}-py2.py3-none-any.whl

BuildArch:      noarch

%global _description %{expand:
%{srcname} provides a generic REST API client for GraceDB and similar
LIGO/Virgo API services. It uses the powerful Requests package for reliable and
high-throughput HTTP connection pooling.}

%description %_description

%package -n python2-%{srcname}
Summary:        %{summary}
Requires:       python2-cryptography
Requires:       python2-requests
Requires:       python2-safe-netrc
Requires:       python2-six
BuildRequires:  python2-devel
BuildRequires:  python2-pip
BuildRequires:  python2-wheel
%{?python_provide:%python_provide python2-%{srcname}}
%description -n python2-%{srcname} %_description

%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}
Requires:       python%{python3_pkgversion}-cryptography
Requires:       python%{python3_pkgversion}-requests
Requires:       python%{python3_pkgversion}-safe-netrc
Requires:       python%{python3_pkgversion}-six
BuildRequires:  python3-devel
BuildRequires:  python%{python3_pkgversion}-pip
BuildRequires:  python%{python3_pkgversion}-wheel
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname} %_description

%prep
%__mkdir_p dist
%__cp %{S:0} dist

%install
%py2_install_wheel %{basename:%{S:0}}
%py3_install_wheel %{basename:%{S:0}}

# Note that there is no %%files section for the unversioned python module
%files -n python2-%{srcname}
%{python2_sitelib}

%files -n python%{python3_pkgversion}-%{srcname}
%{python3_sitelib}

%changelog
* Thu Feb 20 2020 Leo Singer <leo.singer@ligo.org> 0.1.3-1
- New upstream release

* Mon Feb 10 2020 Leo Singer <leo.singer@ligo.org> 0.1.2-1
- New upstream release

* Sun Feb 9 2020 Leo Singer <leo.singer@ligo.org> 0.1.1-2
- Use python3_pkgversion

* Fri Feb 7 2020 Leo Singer <leo.singer@ligo.org> 0.1.1-1
- Initial RPM release
